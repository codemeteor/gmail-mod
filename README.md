# README

## About
The Gmail Mod is an extension for the Chrome, Firefox and Edge web browsers.

It improves Gmail by adding an embedded media-player for video and audio attachments.
The MP4, OGV, WEBM and MOV video file extensions are currently supported. For audio files you should be able to play files with the MP3, OGG and WAV extension.

## Build Instructions

### Required OS
The build instructions have been tested on Chrome OS with Crostini but they should also work on Linux, MacOS and Windows.

### Dependencies
You need the following software installed to follow these build instructions.

* nodejs 10.16.3
* npm 6.13.0
* grunt 1.4.1
* typescript 3.6.3
* jest 24.9.0

### Install the needed node packages
The Gmail Mod extension is written in TypeScript and build using the Grunt build system.
It also requires Jest for running the unit tests.
You will need to install these applications to make sure that the extension builds correctly.

Open a terminal and navigate to the location where you stored the source files, this is the folder that contains the package.json file.

Execute the following commands to install the dependencies.
```
npm install -g grunt typescript jest
npm install
```

### Build

Open a terminal and navigate to the location where you stored the source files, this is the folder that contains the package.json file.

Build the extension with the following command.
```
grunt release
```

To build the beta release execute the following command.
```
grunt release-beta
```

The build packages can be found in the dist/ subfolders.


### Testing Firefox extensions
To easly test the Firefox extension you can use web-ext.

```
npm i -g web-ext
cd ./build/firefox
web-ext run
```