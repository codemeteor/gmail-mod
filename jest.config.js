module.exports = {
      "automock": false,
      "roots": [
        "./src/"
      ],
      "transform": {
        "^.+\.ts?$": "ts-jest"
      },
      //"testRegex": "(/__tests__/.*|(\.|/)(tests|spec))\.ts?$",
      "testRegex": "\/__tests__\/(.*|(\/*.)*)*\.Tests\.ts",
      "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
      ],
    }
