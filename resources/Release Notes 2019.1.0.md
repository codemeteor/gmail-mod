# Release notes 2019.1.0
Gmail-Mod adds extra functionality to the Gmail website.
The current version has the following features:

* Embedded video player for video attachments with file extension .mp4/.webm/.ogv [^1]

[^1]: Note that some files may not play because of an incorrect file extension or unsupported codec by the web browser.

# The future
For the next version the following feature(s) have been planned:

* Playback of audio file attachments.

When you have a great idea for the Gmail-mod don't hesitate to leave a comment or send an email to codemeteor@gmail.com.

# Known issues
In Firefox it is possible to search in the video, but in Chrome it is not. 
This is because the video tag is being dealt with differently in both browsers.

For a more technical explanation please look at this [Stackoverflow](https://stackoverflow.com/a/36101965) thread.

# Support
When you need help using the Gmail-mod, leave a comment or send an email to codemeteor@gmail.com.

If you want to support this project, please consider making a PayPal donation to codemeteor@gmail.com.