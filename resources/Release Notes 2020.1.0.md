# Release notes 2020.1.0
Gmail Mod adds extra functionality to the Gmail website.
The current version has the following features:

* Embedded video player for video attachments with file extension .mp4/.webm/.ogv/.mov [^1]
* Embedded audio player for audio attachments with file extension .wav/mp3/ogg [^1]
* A settings window where you can enable or disable Gmail Mod features.

[^1]: Note that some files may not play because of an incorrect file extension or unsupported codec by the web browser.

# The future
When you have a great idea for the Gmail Mod don't hesitate to leave a comment or send an email to codemeteor@gmail.com.

# Support
When you need help using the Gmail Mod, leave a comment or send an email to codemeteor@gmail.com.
You can also follow us on Twitter @codemeteor or visit https://codemeteor.com.

If you want to support this project, please consider making a PayPal donation using the following url https://www.paypal.me/codemeteor

You can also find the source code on GitLab https://gitlab.com/codemeteor/gmail-mod.