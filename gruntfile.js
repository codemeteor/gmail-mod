
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('./package.json'),
        watch: {
            files: ['./src/**', 'package.json'],
            tasks: ['build'],
            options: {
                spawn: false,
            },
        },
        'string-replace': {
            dist: {
                files: {
                    'src/': 'src/manifest.json',
                },
                options: {
                    replacements: [
                        {
                            pattern: /"version": "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"/ig,
                            replacement: '"version": "<%= pkg.version %>"'
                        },
                        {
                            pattern: /"name": "Gmail Mod Beta"/ig,
                            replacement: '"name": "Gmail Mod"'
                        }
                    ]
                }
            },
            beta: {
                files: {
                    'src/': 'src/manifest.json',
                },
                options: {
                    replacements: [
                        {
                            pattern: /"version": "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"/ig,
                            replacement: '"version": "<%= pkg.version %>"'
                        },
                        {
                            pattern: /"name": "Gmail Mod"/ig,
                            replacement: '"name": "Gmail Mod Beta"'
                        }
                    ]
                }
            },
            firefox: {
                files: {
                    './': 'build/firefox/manifest-v2.json',
                },
                options: {
                    replacements: [
                        {
                            pattern: /"manifest_version": 2/ig,
                            replacement: '"manifest_version": 2,"browser_specific_settings": {"gecko": {"id": "{f670f216-9980-42b1-84a3-4c511c27dc3a}","strict_min_version": "57.0"}}'
                        },
                        {
                            pattern: /"incognito": "split"/ig,
                            replacement: '"incognito": "spanning"'
                        }
                    ]
                }
            },
            "firefox-beta": {
                files: {
                    './': 'build/firefox/manifest-v2.json',
                },
                options: {
                    replacements: [
                        {
                            pattern: /"manifest_version": 2/ig,
                            replacement: '"manifest_version": 2,"browser_specific_settings": {"gecko": {"id": "{d84266a5-c489-4efe-afed-36223a69b9a4}","strict_min_version": "57.0","update_url": "https://codemeteor.com/gmailmod/beta/firefox/updates.json"}}'
                        },
                        {
                            pattern: /"incognito": "split"/ig,
                            replacement: '"incognito": "spanning"'
                        }
                    ]
                }
            }
        },
        browserify: {
            default: {
                src: 'src/Main.ts',
                dest: 'build/js/gmail-mod.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    },
                    plugin: [
                        ['tsify', { noImplicitAny: true }]
                    ]
                }
            },
            optionsscript: {
                src: 'src/Options.ts',
                dest: 'build/js/options.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    },
                    plugin: [
                        ['tsify', { noImplicitAny: true }]
                    ]
                }
            },
            backgroundscript: {
                src: 'src/Background.ts',
                dest: 'build/js/background.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    },
                    plugin: [
                        ['tsify', { noImplicitAny: true }]
                    ]
                }
            },
            videoplayerscript: {
                src: 'src/VideoPlayer.ts',
                dest: 'build/js/VideoPlayer.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    },
                    plugin: [
                        ['tsify', { noImplicitAny: true }]
                    ]
                }
            }
        },
        copy: {
            'build-chrome': {
                files: [
                    { expand: true, cwd: './src/', src: ['**', '!**/*.svg', '!**/*.ts', '!**/__tests__/**', '!**/Models/**', '!**/Types/**'], dest: './build/chrome' },
                    { expand: true, cwd: './build/js/', src: ['**'], dest: './build/chrome' },
                ]
            },
            'build-firefox': {
                files: [
                    { expand: true, cwd: './src', src: ['**', '!**/*.svg', '!**/*.ts', '!**/__tests__/**', '!**/Models/**', '!**/Types/**'], dest: './build/firefox' },
                    { expand: true, cwd: './build/js/', src: ['**'], dest: './build/firefox' },
                ]
            },
            'build-edge': {
                files: [
                    { expand: true, cwd: './src', src: ['**', '!**/*.svg', '!**/*.ts', '!**/__tests__/**', '!**/Models/**', '!**/Types/**'], dest: './build/edge' },
                    { expand: true, cwd: './build/js/', src: ['**'], dest: './build/edge' },
                ]
            },
            'build-src': {
                files: [
                    { expand: true, cwd: './', src: ['**', '!**/.tscache/**', '!**/.vscode/**', '!**/build/**', '!**/dist/**', '!**/node_modules/**', '!**/resources/**', '!**/*.svg', '!**/__tests__/**'], dest: './build/src' },
                ]
            },
        },
        clean: {
            build: {
                src: ['build/**']
            },
            dist: {
                src: ['dist/**']
            },
            manifest: {
                src: [
                    'build/chrome/manifest-v2.json',
                    'build/edge/manifest-v2.json',
                    'build/firefox/manifest.json']
            }
        },
        zip: {
            'dist-chrome': {
                cwd: 'build/chrome/',
                src: ['./build/chrome/**'],
                dest: './dist/chrome/gmail-mod.zip'
            },
            'dist-firefox': {
                cwd: 'build/firefox/',
                src: ['./build/firefox/**'],
                dest: './dist/firefox/gmail-mod.zip'
            },
            'dist-edge': {
                cwd: 'build/edge/',
                src: ['./build/edge/**'],
                dest: './dist/edge/gmail-mod.zip'
            },
            'dist-src': {
                cwd: 'build/src/',
                src: ['./build/src/**'],
                dest: './dist/src/gmail-mod-src.zip'
            }
        },
        rename: {
            manifest: {
              files: [
                    {src: ['./build/firefox/manifest-v2.json'], dest: './build/firefox/manifest.json'},
                  ]
            }
        }
    })

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-zip');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-rename');

    grunt.registerTask('default', ['build-dev', 'watch']);
    grunt.registerTask('set-version', ['string-replace:dist']);
    grunt.registerTask('set-version-beta', ['string-replace:beta']);


    grunt.registerTask('build-dev', ['readpkg', 'set-version', 'clean:build', 'browserify', 'copy:build-chrome', 'copy:build-firefox', 'copy:build-edge', 'string-replace:firefox', 'clean:manifest', 'rename:manifest'])
    grunt.registerTask('build', ['readpkg', 'set-version', 'clean:build', 'browserify', 'copy', 'string-replace:firefox', 'clean:manifest', 'rename:manifest' ])
    grunt.registerTask('build-beta', ['readpkg', 'set-version-beta', 'clean:build', 'browserify', 'copy', 'string-replace:firefox-beta', 'clean:manifest', 'rename:manifest' ])
    grunt.registerTask('release', ['clean', 'build', 'zip']);
    grunt.registerTask('release-beta', ['clean', 'build-beta', 'zip']);
    grunt.registerTask('readpkg', 'Read in the package.json file', function () {
        grunt.config.set('pkg', grunt.file.readJSON('./package.json'));
    });
}