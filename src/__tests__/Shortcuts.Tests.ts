import { Shortcuts } from '../Shortcuts';
jest.dontMock("fs");
import fs from "fs";

test("Triggering the key press event for the ESC key returns true.", () => {
  let html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;

  let shortcuts = new Shortcuts();

  var result = false;
  shortcuts.add("Escape", () => {
    result = true;
  });

  var keyEvent = new KeyboardEvent('keyup', { key: 'Escape' })
  document.dispatchEvent(keyEvent);

  expect(result).toBe(true);
});

test("Triggering the key press event for the ESC key after removing the shortcut results in false.", () => {
  let html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;

  let shortcuts = new Shortcuts();

  var result = false;
  shortcuts.add("Escape", () => {
    result = true;
  });

  var keyEvent = new KeyboardEvent('keyup', { key: 'Escape' });
  document.dispatchEvent(keyEvent);

  expect(result).toBe(true);

  shortcuts.remove("Escape");

  result = false;
  var keyEvent = new KeyboardEvent('keyup', { key: 'Escape' });
  document.dispatchEvent(keyEvent);

  expect(result).toBe(false);
})