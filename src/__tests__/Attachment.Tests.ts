import { Attachment } from "../Attachment";
import { AttachmentType } from "../Types/AttachmentType";
jest.dontMock("fs");
import fs from "fs";


test("Returns with at least 1 attachment", () => {
  let htmlFile = fs.readFileSync('./src/__tests__/html/trailer.html', 'utf-8').toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result.length).toBeGreaterThanOrEqual(1);
});

test("The first attachment is a video attachment", () => {
  let htmlFile = fs.readFileSync('./src/__tests__/html/beer-with-friends.html', 'utf-8').toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Type).toEqual(AttachmentType.video);
});

test("The first attachment is a audio attachment", () => {
  let htmlFile = fs.readFileSync("./src/__tests__/html/audio.html", "utf-8").toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Type).toEqual(AttachmentType.audio);
})

test("The first attachment has a .mov extension", () => {
  let htmlFile = fs.readFileSync('./src/__tests__/html/trailer.html', 'utf-8').toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Extension).toEqual(".mov")
});

test("The first attachment has a .avi extension", () => {
  let htmlFile = fs.readFileSync('./src/__tests__/html/test-video.html', 'utf-8').toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Extension).toEqual(".avi")
});

test("The first attachment has as url https://mail.google.com/mail/u/0?ui=2&ik=763a943965&attid=0.1&permmsgid=msg-f:1622348022257575122&th=1683bcee40b864d2&view=att&disp=safe&realattid=f_jqrpw5h10", () => {
  var html = fs.readFileSync('./src/__tests__/html/beer-with-friends.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);
  
  let result = attachment.findAttachments();
  expect(result[0].Url).toEqual("https://mail.google.com/mail/u/0?ui=2&ik=763a943965&attid=0.1&permmsgid=msg-f:1622348022257575122&th=1683bcee40b864d2&view=att&disp=safe&realattid=f_jqrpw5h10")
});

test("The first attachment has as title 'Beer with Friends.mp4'", () => {
  let htmlFile = fs.readFileSync('./src/__tests__/html/beer-with-friends.html', 'utf-8').toString();
  document.documentElement.innerHTML = htmlFile;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Title).toEqual('Beer with Friends.mp4');
})

test("The first attachment has as url https://mail.google.com/mail/u/0?ui=2&ik=763a943965&attid=0.1&permmsgid=msg-a:r6472879918969334598&th=1668fd6f85c7ce7e&view=att&disp=safe&realattid=1668fd6be69721e4f761", () => {
  var html = fs.readFileSync('./src/__tests__/html/trailer.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Url).toEqual("https://mail.google.com/mail/u/0?ui=2&ik=763a943965&attid=0.1&permmsgid=msg-a:r6472879918969334598&th=1668fd6f85c7ce7e&view=att&disp=safe&realattid=1668fd6be69721e4f761");
});

test("The first attachment has the title 'trailer_480p.mov'", () => {
  var html = fs.readFileSync('./src/__tests__/html/trailer.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Title).toEqual("trailer_480p.mov");
});

test("The file format is supported", () => {
  var html = fs.readFileSync('./src/__tests__/html/beer-with-friends.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Supported).toEqual(true);
});

test("The file format is not supported", () => {
  var html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  expect(result[0].Supported).toEqual(false);
});

test("Begins with <span class=\"aZo a5r N5jrZb\" download_url=\"video/quicktime:trailer_480p.mov:https://mail.google.com/mail/u/0/https://mail.google.com/mail/u/0?ui=2&ik=763a943965&attid=0.1&permmsgid=msg-a:r6472879918969334598&th=1668fd6f85c7ce7e&view=att&disp=safe&realattid=1668fd6be69721e4f761\" draggable=\"true\">", () => {
  var html = fs.readFileSync('./src/__tests__/html/trailer.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);
  
  let result = attachment.findAttachments();
  expect(result[0].Element.outerHTML.trim().startsWith("<span class=\"aZo a5r N5jrZb\" download_url=\"video/quicktime:trailer_480p.mov:https://mail.google.com/mail/u/0/https://mail.google.com/mail/u/0?ui=2&amp;ik=763a943965&amp;attid=0.1&amp;permmsgid=msg-a:r6472879918969334598&amp;th=1668fd6f85c7ce7e&amp;view=att&amp;disp=safe&amp;realattid=1668fd6be69721e4f761\" draggable=\"true\">")).toEqual(true);
});

test("Adds play button to the attachment for video attachment.", () => {
  var html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  expect(document.querySelector(".PlayButton")).not.toBeNull();
});

test("Adds play button to the attachment for audio attachment.", () => {
  var html = fs.readFileSync('./src/__tests__/html/audio.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  expect(document.querySelector(".PlayButton")).not.toBeNull();
});

test("addPlayButton to attachment without disp=safe url won't add button.", () => {
  var html = fs.readFileSync('./src/__tests__/html/test-video-unsafe.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  expect(document.querySelector(".PlayButton")).toBeNull();
});

test("addPlayButton adds url data to the playbutton", () => {
  var html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  expect(document.querySelector(".VideoPlayer"));
});

test("The play button has a data url property", () => {
  let html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  let playButton = <HTMLDivElement>document.querySelector(".PlayButton");
  
  expect(playButton.getAttribute("data-url")).not.toBeNull();
});

test("The play button has a data title property", () => {
  let html = fs.readFileSync('./src/__tests__/html/test-video.html').toString();
  document.documentElement.innerHTML = html;
  let attachment = new Attachment(document);

  let result = attachment.findAttachments();
  attachment.addPlayButton(result[0]);
  let playButton = <HTMLDivElement>document.querySelector(".PlayButton");
  
  expect(playButton.getAttribute("data-title")).not.toBeNull();
});