export class OptionCategories {
  public static categories = [
    {
      name: "Attachments", features: [
        {
          label: "Embedded video player",
          name: "video-player",
          type: "checkbox",
          defaultValue: true
        },
        {
          label: "Embedded audio player",
          name: "audio-player",
          type: "checkbox",
          defaultValue: true
        }]
    },
    /*{
      name: "Misc", features: [
        {
          label: "Display What's new wizard",
          name: "show_wizard",
          type: "radio",
          defaultValue: 1,
          values: [
            { label: "After each new release", value: 1 },
            { label: "Never", value: 2 }]
        }]
    }*/];
}