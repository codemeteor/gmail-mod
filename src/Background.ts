import { AttachmentType } from "./Types/AttachmentType";

export default class Background {
    private _tabID = 0;
    private _requesturl = "";

    constructor() {

        chrome.runtime.onMessage.addListener((request: any, sender: any, sendResponse: any) => {
            if (request.type == AttachmentType.video) {
                this.createMediaPlayerTabAndGetData(request, sender, sendResponse)
            }
            if (request.type == AttachmentType.audio) {
                fetch(request.url, { mode: 'cors' }).then(
                    response => response.arrayBuffer()
                ).then(
                    (arrayBuffer) => {
                            let newArray = Array.from(new Uint8Array(arrayBuffer))
                            var data = {file: newArray}
                            var string = JSON.stringify(data)
                            sendResponse(string)
                        }
                );
                return true;
            }
        })

        chrome.tabs.onUpdated.addListener((tabId, info) => {
            if (this._tabID == tabId && info.status === 'complete') {
                this.getMediaData(this._requesturl);
            }
        });
    }

    private async createMediaPlayerTabAndGetData(request: any, sender: any, sendResponse: any) {
        let pages: Array<string> = [];
        pages[AttachmentType.audio] = "AudioPlayer.html";
        pages[AttachmentType.video] = "VideoPlayer.html";
        this._requesturl = request.url;

        chrome.tabs.create({ url: pages[request.type] }, (event: any) => {
            this._tabID = event.id;

            sendResponse();
        });
    }

    private async getMediaData(mediaUrl: string) {
        chrome.tabs.sendMessage(this._tabID, { mediadata: mediaUrl });
    }

}


const background = new Background();