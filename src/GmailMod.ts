import { Logging } from './Logging';
import { AttachmentWatcher } from './AttachmentWatcher';
import { Settings } from './Settings';

/**
 * This class contains the main loop
 */
export class GmailMod {
  private attachmentWatcher: AttachmentWatcher  = new AttachmentWatcher();
  public static port: chrome.runtime.Port;

  /**
   * Create new instance of the GmailMod class
   */
  constructor() {
    document.addEventListener("load", () => { this.addFontAwesome });
    Settings.loadOptions(() => { this.startMain(); });
  }

  private startMain() {
    setInterval(() => { this.main() }, 500);
  }

  
  /**
   * Start the main loop
   */
  public main() {
    Logging.logOnce("Entering main loop.");
    this.attachmentWatcher.attachmentCheck();
  }

  private addFontAwesome() {
    Logging.log("Add FontAwesome.")
    let head = document.head;
    let link = document.createElement("link");

    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = "https://use.fontawesome.com/releases/v5.5.0/css/all.css";
    link.integrity = "sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU";
    link.crossOrigin = "anonymous";

    head.appendChild(link);
  }
}