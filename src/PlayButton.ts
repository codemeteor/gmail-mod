import { Logging } from "./Logging";
import { AttachmentModel } from "./Models/AttachmentModel";
import { AttachmentType } from "./Types/AttachmentType";
import { Util } from "./Util";

export interface IPlayButton extends HTMLDivElement {
    audioContext: AudioContext;
    audioLoaded: boolean;
    audioPlaying: boolean;
    arrayBuffer: ArrayBuffer;
    audioBuffer: AudioBuffer;
    audioSource: AudioBufferSourceNode;
    changeToPlay: Function;
    changeToPause: Function;
    changeToLoading: Function;
    addSlider: Function;
    slider: HTMLInputElement;
}

export default class PlayButton {
    private playButton: IPlayButton = <IPlayButton>document.createElement("div");

    constructor(attachment: AttachmentModel, clickAction: Function) {
        if (attachment.Supported != false) {
            var buttonFields = attachment.Element.getElementsByClassName("aQw");
            var playButtons = buttonFields[0].getElementsByClassName("PlayButton");

            let tooltips: { [attachmentType: number]: string } = {};
            tooltips[AttachmentType.audio] = "Play audio";
            tooltips[AttachmentType.video] = "Play video";

            if (playButtons.length === 0 && (attachment.Url.indexOf("disp=safe") !== -1 || attachment.Url.indexOf("docs.googleusercontent.com/") !== -1)) {
                this.playButton = Object.assign(<HTMLDivElement>Util.createElementFromHtml(
                    `<div\
     class="PlayButton wtScjd J-J5-Ji aYr playButton"\
     role="button"\
     aria-label="${tooltips[attachment.Type]}"\
     data-tooltip-class="a1V"\
     aria-hidden="false"\
     data-tooltip="${tooltips[attachment.Type]}"\
     data-url="${attachment.Url.replace(/&/g, "&amp;")}"\
     data-title="${attachment.Title.replace(/&/g, "&amp;")}"\
     tabindex="0">\
     </div>`
                ));


                this.playButton.addEventListener("click", (e: any) => {
                    clickAction(e);
                });
                buttonFields[0].appendChild(this.playButton);
                Logging.log(`Added play button to ${attachment.Title}.`)
            }
        }
        this.playButton.changeToPlay = PlayButton.changeToPlay;
        this.playButton.changeToPause = PlayButton.changeToPause;
        this.playButton.changeToLoading = PlayButton.changeToLoading;
        this.playButton.addSlider = PlayButton.addSlider;
    }

    static changeToPlay(playButton: IPlayButton) {
        console.log(playButton.className);
        playButton.className = playButton.className.replace(/pauseButton/g, "playButton");
        playButton.className = playButton.className.replace(/loadingButton/g, "playButton");
    }

    static changeToPause(playButton: IPlayButton) {
        playButton.className = playButton.className.replace(/playButton/g, "pauseButton");
        playButton.className = playButton.className.replace(/loadingButton/g, "pauseButton");
    }

    static changeToLoading(playButton: IPlayButton) {
        playButton.className = playButton.className.replace(/playButton/g, "loadingButton");
        playButton.className = playButton.className.replace(/pauseButton/g, "loadingButton");
    }

    static addSlider(playButton: IPlayButton, max: number) {
        playButton.slider = <HTMLInputElement>document.createElement('input');
        playButton.slider.type = "range";
        playButton.slider.max = `${max}`;
        playButton.parentElement?.appendChild(playButton.slider);
    }
}