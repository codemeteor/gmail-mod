/**
 * Data structure used to define a keyboard shortcut.
 */
export class ShortcutModel {

  /**
   * Create new instnace of the ShortcutModel class.
   * @param Key The key that needs to be registred to the shortcut event.
   * @param Callback The function that needs to be called when the key is pressed.
   */
  constructor(
    public Key: string, 
    public Callback: Function) {}
}