import { AttachmentType } from '../Types/AttachmentType';

/**
 * Data structure used to define an attachment.
 */
export class AttachmentModel {

  /**
   * Create a new instance of the AttachementModel class.
   * @param Type The definition of the attachment type.
   * @param Extension The extension from the filename.
   * @param Url The url to the attachement.
   * @param Title The title of the attachment.
   * @param Supported Is the file type supported by the Gmail Mod.
   * @param Element The HTML element that is the attachement.
   */
  constructor(
    public Type: AttachmentType = AttachmentType.unknown,
    public Extension: string = "",
    public Url: string = "",
    public Title: string = "",
    public Supported: boolean = false,
    public Element: HTMLElement = document.createElement('template')
  ) {}
}