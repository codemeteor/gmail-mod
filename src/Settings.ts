import { OptionCategories } from './OptionCategories';

export class Settings {
  public static options: any;

  public static loadOptions(callback: Function) {
    let defaultValues: { [id: string]: any; } = {};
    for (let category of OptionCategories.categories) {
      for (let feature of category.features) {
        defaultValues[`${feature.name}`] = feature.defaultValue;
      }
    }

    chrome.storage.sync.get(
      defaultValues, (items) => {
        Settings.options = items;
        callback(items);
      }
    )
  }

}