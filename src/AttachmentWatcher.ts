import { Logging } from './Logging';
import { Attachment } from './Attachment';
import { AttachmentModel } from './Models/AttachmentModel';
import { AttachmentType } from './Types/AttachmentType';
import { Settings } from './Settings';

/**
 * Class that searches for attachments and adds extra functionality to them.
 */
export class AttachmentWatcher {

    /**
     * Checks if there is a video attachment in the current DOM that needs a play button.
     */
    public attachmentCheck() {
        Logging.logOnce("Checking for video attachments.")
        let attachment: Attachment = new Attachment(document);
        let attachments: AttachmentModel[] = attachment.findAttachments();

        attachments.forEach(attachmentData => {
            this.addMediaPlayButtons(attachment, attachmentData);
        });
    }

    private addMediaPlayButtons(attachment: Attachment, attachmentData: AttachmentModel) {
        if (this.isSupportedFormat(attachmentData)) {
            attachment.addPlayButton(attachmentData);
        }
    }

    private isSupportedFormat(attachmentData: AttachmentModel) {
        return (attachmentData.Type === AttachmentType.video &&
            attachmentData.Supported === true && Settings.options["video-player"] == true) ||
            (attachmentData.Type === AttachmentType.audio &&
                attachmentData.Supported === true && Settings.options["audio-player"] == true)
    }
}