/**
 * This class provides logging functionality.
 */
export class Logging {
    private static oneTimeLog: Array<string>;

    static getCallerName() : string {
        let stackTrace: string = <string>(new Error()).stack; // Only tested in latest FF and Chrome
        stackTrace = stackTrace.replace(/^Error\s+/, ''); // Sanitize Chrome
        let callerName: string = stackTrace.split("\n")[2]; // second item is caller
        if (callerName.indexOf("logOnce") !== -1) {
            callerName = stackTrace.split("\n")[3]; // third item is caller            
        }
        callerName = callerName.replace(/^\s+at Object./, ''); // Sanitize Chrome
        callerName = callerName.replace(/^\s+at /, ''); // Sanitize Chrome
        callerName = callerName.replace(/ \(.+\)$/, ''); // Sanitize Chrome
        callerName = callerName.replace(/\@.+/, ''); // Sanitize Firefox
        callerName = callerName.replace(/^\[[0-9]*\]<\//, ''); // Sanitize Firefox
        callerName = callerName.replace(/\.prototype\./, '.'); // Sanitize Firefox

        return callerName;
    }

    /**
     * Log the message.
     * @param {string} message The message to log.
     */
    static log(message: string) {
        let callerName: string = this.getCallerName();

        console.log(`Gmail Mod - ${callerName} : ${message}`);
    }

    /**
     * Log the given message only once during the application runtime.
     * @param {string} message The message to log.
     */
    static logOnce(message: string) {
        if (Logging.oneTimeLog === undefined) {
            Logging.oneTimeLog = new Array<string>();
        }
        if (Logging.oneTimeLog.indexOf(message) === -1) {
            this.log(message)
            this.oneTimeLog.push(message);
        }
    }

};