import { OptionCategories } from './OptionCategories';
import { Settings } from './Settings';
import { Util } from './Util';
export class Options {

  
  private showControls: { [id: string]: Function; } = {};
  private restoreControls: { [id: string]: Function; } = {};
  

  constructor() {
    this.showControls["radio"] = this.showRadio;
    this.showControls["checkbox"] = this.showCheckbox;

    this.restoreControls["radio"] = this.restoreRadio;
    this.restoreControls["checkbox"] = this.restoreCheckbox;
    
    this.showOptions();
    Settings.loadOptions((items: any) => {
      this.loadItems(items);
     });
  }

  private showOptions() {
    let HtmlString: string = `
    <table class="options-table">
      <tbody>`;
    for (let category of OptionCategories.categories) {
      HtmlString += `
      <tr>
        <td>
          <h2>${category.name}</h2>
        </td>
      </tr>
      `;
      for (let feature of category.features) {
        HtmlString += this.showControls[feature.type](feature);
      }
    }
    HtmlString += `
        <tr>
          <td class="save-button-cell">
            <button id="saveOptions">Save</button>
          </td>
        </tr>
      </tbody>
    </table>
    `;

    let htmlElements = Util.createElementFromHtml(HtmlString);
    console.log(document);
    document.getElementsByTagName("body")[0].appendChild(htmlElements);
    let saveButton = <HTMLButtonElement>document.getElementById("saveOptions");
    saveButton.addEventListener("click", () => { this.saveOptions();})
  }

  private showRadio(data: any) {
    let htmlString  = `
    <tr>
      <td>${data.label}</td>
    </tr>`;
    for (let value of data.values) {
      htmlString += `
      <tr>
        <td>
          <input type="radio" name="${data.name}" id="${data.name + value.value}" value="${value.value}">
          <label for="${data.name + value.value}">${value.label}</label>
        </td>
      </tr>
      `;
      return htmlString;
    }
  }

  private saveOptions() {
    let valuesToSave: { [id: string]: any; } = {};
    for (let category of OptionCategories.categories) {
      for (let feature of category.features) {
        if (feature.type == `checkbox`) {
          let checkbox = <HTMLInputElement>document.getElementById(feature.name);
          valuesToSave[`${feature.name}`] = checkbox.checked;
        }
        if (feature.type == `radio`) 
        {
          let radioButtons  = document.getElementsByName(feature.name);
          for (let index = 0; index != radioButtons.length; index++) {
            let radio = <HTMLInputElement>radioButtons[index];
            if (radio.checked == true)
            valuesToSave[`${feature.name}`] = radio.value;
          }
        }
      }
    }


    chrome.storage.sync.set(valuesToSave
      , () => {
        window.close();
    });
  }

  private showCheckbox(data: any) {
    return `
    <tr>
      <td>
        <input type="checkbox" id="${data.name}"><label for="${data.name}">${data.label}</label>
      </td>
    </tr>
  `;
  }

  private loadItems(items: { [id: string]: any; }) {
    for (let category of OptionCategories.categories) {
      for (let feature of category.features) {
        this.restoreControls[feature.type](items, feature);
      }
    }
  }


  private restoreRadio(items: { [id: string]: any; }, feature: any) {
    let radio = <HTMLInputElement>document.getElementById(feature.name + items[feature.name]);
          radio.checked = true;
  }
  private restoreCheckbox(items: { [id: string]: any; }, feature: any) {
    let checkbox = <HTMLInputElement>document.getElementById(feature.name);
          checkbox.checked = items[feature.name];
  }
}

let options = new Options();
