import { AttachmentType } from './Types/AttachmentType';
import { AttachmentModel } from './Models/AttachmentModel';
import { Util } from './Util'
import { Logging } from './Logging';
import PlayButton, { IPlayButton } from './PlayButton';
import { AudioPlayer } from './AudioPlayer';

/**
 * THis class is used to find attachments on the Gmail page and add extra fubnctionality to them.
 */
export class Attachment {
  //private readonly supportedFormats = ".mp4;.webm;.ogv;"
  private supportedFormats: { [extension: string]: AttachmentType } = {};

  private playerActions: { [attachmentType: string]: any } = {};

  /**
   * Create a new instance of the Attachment class
   * @param HtmlDocument The HTML document to search for attachment elements.
   */
  constructor(private HtmlDocument: HTMLDocument = document) {
    this.supportedFormats['.mp4'] = AttachmentType.video;
    this.supportedFormats['.webm'] = AttachmentType.video;
    this.supportedFormats['.ogv'] = AttachmentType.video;
    this.supportedFormats['.mov'] = AttachmentType.video;
    this.supportedFormats['.wav'] = AttachmentType.audio;
    this.supportedFormats['.mp3'] = AttachmentType.audio;
    this.supportedFormats['.ogg'] = AttachmentType.audio;

    this.playerActions[AttachmentType.video] = this.requestVideoPlayer;
    this.playerActions[AttachmentType.audio] = this.requestAudioPlayer;
  }

  private requestVideoPlayer(e: any) {

    chrome.runtime.sendMessage({ url: e.target.getAttribute("data-url"), type: AttachmentType.video });
  }


  private async requestAudioPlayer(e: any) {
    let playButton: IPlayButton = Object.assign(<HTMLDivElement>e.currentTarget);
    let audioPlayer = new AudioPlayer(playButton);
    await audioPlayer.createPlayer(e.target.getAttribute("data-url"));
  }

  /**
   * Search for all attachments inside the HTML document.
   * @returns Returns a list of attachment data.
   */
  public findAttachments(): AttachmentModel[] {
    let attachments: AttachmentModel[] = new Array();
    let attachmentElements: HTMLCollectionOf<Element> = this.getAttachmentsInMessageView();

    this.extractAttachmentsData(attachmentElements, attachments);
    return attachments;
  }

  private playButton: PlayButton = new PlayButton(new AttachmentModel(), () => {});

  /**
   * Adds a play button to the attachment element.
   * @param attachment The attachment data that we use to generate the play button.
   */
  public addPlayButton(attachment: AttachmentModel) {
    this.playButton = new PlayButton(attachment, this.playerActions[attachment.Type]);
  }

  private getAttachmentsInMessageView(): HTMLCollectionOf<Element> {
    return this.HtmlDocument.getElementsByClassName("aZo");
  }

  private extractAttachmentsData(attachmentElements: HTMLCollectionOf<Element>, attachments: AttachmentModel[]) {
    for (let index = 0; index !== attachmentElements.length; index++) {
      let attachment: AttachmentModel = this.extractAttachmentData(attachmentElements, index);
      attachments.push(attachment);
    }
  }

  private extractAttachmentData(attachmentElements: HTMLCollectionOf<Element>, index: number) {
    let attachment: AttachmentModel = new AttachmentModel();

    let attachmentElement = this.getAttachmentElement(attachmentElements, index);
    let anchorElement = this.getAnchorElement(attachmentElement);
    attachment.Element = <HTMLElement>attachmentElement;
    attachment.Url = this.getAttachmentUrl(anchorElement);

    let fileElements = this.getFileElements(anchorElement);
    this.extractTitleFromFileElement(fileElements, attachment);
    this.extractFileExtension(attachment);
    this.isSupported(attachment);
    this.setAttachmentType(attachment);
    return attachment;
  }

  private getAttachmentElement(attachmentElements: HTMLCollectionOf<Element>, index: number) {
    return attachmentElements[index];
  }

  private getAnchorElement(attachmentElement: Element) {
    return attachmentElement.getElementsByTagName("a")[0];
  }

  private getAttachmentUrl(anchorElement: HTMLAnchorElement) {
    return <string>anchorElement.getAttribute("href");
  }

  private getFileElements(anchorElement: HTMLAnchorElement) {
    return anchorElement.getElementsByClassName("aV3");
  }

  private extractTitleFromFileElement(fileElements: HTMLCollectionOf<Element>, attachment: AttachmentModel) {
    if (fileElements.length === 1) {
      let fileName = <string>fileElements[0].textContent;
      attachment.Title = fileName.trim();
    }
  }

  private extractFileExtension(attachment: AttachmentModel) {
    let indexOfDot = attachment.Title.lastIndexOf(".");
    attachment.Extension = attachment.Title.substr(indexOfDot).toLowerCase();
  }

  private isSupported(attachment: AttachmentModel) {
    if (this.supportedFormats[attachment.Extension]) {
      attachment.Supported = true;
    }
    else {
      attachment.Supported = false;
    }
  }

  private setAttachmentType(attachment: AttachmentModel) {
    if (this.supportedFormats[attachment.Extension]) {
      attachment.Type = this.supportedFormats[attachment.Extension];
    }
    else {
      attachment.Type = AttachmentType.unknown;
    }
  }
}