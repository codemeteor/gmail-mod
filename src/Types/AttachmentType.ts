/**
 * Defines the type of attachment.
 */
export enum AttachmentType {
  unknown = 1,
  video = 2,
  audio = 3
}