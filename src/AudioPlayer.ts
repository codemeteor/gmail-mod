import { Shortcuts } from "./Shortcuts";
import { Logging } from "./Logging";
import { IPlayButton } from "./PlayButton";
import { request } from "http";
import { Util } from "./Util";
import { AttachmentType } from "./Types/AttachmentType";

/**
 * Create a audio player.
 */
export class AudioPlayer {
	constructor(private _playButton: IPlayButton) { }
	private offset: number = 0;
	private sliderInterval: ReturnType<typeof setInterval> = setInterval(() => { }, 1000);

	public async createPlayer(url: string) {
		if (
			this._playButton.audioLoaded === undefined ||
			this._playButton.audioLoaded === false
		) {
			await this.loadAudio(url);
			this._playButton.audioSource.start();
		} else if (
			this._playButton.audioLoaded === true &&
			this._playButton.audioPlaying === true
		) {
			this._playButton.audioContext.suspend();
			this._playButton.audioPlaying = false;
			this._playButton.changeToPlay(this._playButton);
		} else if (
			this._playButton.audioLoaded === true &&
			this._playButton.audioPlaying === false
		) {
			this._playButton.audioContext.resume();
			this._playButton.audioPlaying = true;
			this._playButton.changeToPause(this._playButton);
		}
	}

	private stopProgressBarUpdate() {
		console.log(`Stop progressbar update interval ${this.sliderInterval}`)
		clearInterval(this.sliderInterval);
	}

	private startProgressBarUpdate() {
		this.sliderInterval = setInterval(() => {

			let currentTime = <number>(
				this._playButton.audioContext.getOutputTimestamp().contextTime
			);

			if (this._playButton.audioPlaying == true) {
				this._playButton.slider.value = `${this.offset + currentTime * 100}`;
			}

		}, 100);
		console.log(`Started update progress bar ${this.sliderInterval}`);
	}

	private async loadAudio(url: string) {
		return new Promise((resolve) => {
			this._playButton.changeToLoading(this._playButton);

			chrome.runtime.sendMessage({ url: url, type: AttachmentType.audio }, async (response) => {
				let dataArray = JSON.parse(response);
				var arrayBuffer = new Uint8Array(dataArray.file).buffer
				await this.dataLoaded(arrayBuffer)
				resolve('resolved');
			})
		})
	}

	private async dataLoaded(response: ArrayBuffer) {
		this._playButton.audioContext = new AudioContext();
		this._playButton.arrayBuffer = response;
		try {
			this._playButton.audioBuffer =
				await this._playButton.audioContext.decodeAudioData(
					this._playButton.arrayBuffer
				);
		} catch {
			console.error("This file format is not supported by your browser.");
			Util.showError("This file format is not supported by your browser.");
			this._playButton.changeToPlay(this._playButton);
			return;
		}



		this._playButton.audioSource =
			this._playButton.audioContext.createBufferSource();
		this._playButton.audioSource.buffer = this._playButton.audioBuffer;
		this._playButton.audioSource.connect(
			this._playButton.audioContext.destination
		);

		this._playButton.audioSource.addEventListener("ended", () => {
			this.handleAudioEnded();
		});

		this._playButton.addSlider(
			this._playButton,
			this._playButton.audioBuffer.duration * 100
		);
		this.stopProgressBarUpdate();

		this.startProgressBarUpdate();

		this._playButton.slider.addEventListener("input", () => {
			this.handleSeek();
		});
		this._playButton.audioLoaded = true;
		this._playButton.audioPlaying = true;
		this._playButton.changeToPause(this._playButton);
	}

	private handleSeek() {
		this._playButton.audioPlaying = false;
		this.offset = parseFloat(this._playButton.slider.value);
		this._playButton.audioContext.close();
		this._playButton.audioContext = new AudioContext();
		this._playButton.audioSource =
			this._playButton.audioContext.createBufferSource();
		this._playButton.audioSource.buffer = this._playButton.audioBuffer;
		this._playButton.audioSource.connect(
			this._playButton.audioContext.destination
		);
		this._playButton.audioSource.start(0, this.offset / 100);
		this._playButton.audioPlaying = true;
		this._playButton.changeToPause(this._playButton);
		this._playButton.audioSource.addEventListener("ended", () => {
			this.handleAudioEnded();
		});
	}

	private handleAudioEnded() {
		this.offset = 0;
		this._playButton.audioContext.close();
		this._playButton.audioContext = new AudioContext();
		this._playButton.audioSource =
			this._playButton.audioContext.createBufferSource();
		this._playButton.audioSource.buffer = this._playButton.audioBuffer;
		this._playButton.audioSource.connect(
			this._playButton.audioContext.destination
		);
		this._playButton.audioSource.start();
		this._playButton.audioContext.suspend();
		this._playButton.audioPlaying = false;
		this._playButton.changeToPlay(this._playButton);
		this._playButton.audioSource.addEventListener("ended", () => {
			this.handleAudioEnded();
		});
	}
}
