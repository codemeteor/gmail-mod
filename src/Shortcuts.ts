import { ShortcutModel } from "./Models/ShortcutModel";
import { Logging } from "./Logging";

/**
 * Adds global shortcuts to a website
 */
export class Shortcuts {
  private shortcuts: Array<ShortcutModel>;

  /**
   * Create a new instance of the shortcuts class.
   */
  constructor() {
    document.addEventListener("keyup", (e) => {
      this.checkShortcuts(e);
    });
    this.shortcuts = new Array<ShortcutModel>();
  }

  /**
   * Add a new global keyboard shortcut.
   * @param key The key press that will result to the execution of the defined callback function.
   * @param callback The function that will be executed when the shortcut key has been pressed.
   */
  public add(key : string, callback: Function) {
    var shortcut = new ShortcutModel(key, callback);
    this.shortcuts.push(shortcut);
    Logging.log(`Created new shortcut for "${key}"`)
  }

  /**
   * Remove the global shortcut.
   * @param key The key for which the shortcut needs to be removed.
   */
  public remove(key: string) {
    let shortcutToRemove: ShortcutModel | null = null;
    for (var shortcut of this.shortcuts) {
      if (shortcut.Key == key) {
        shortcutToRemove = shortcut;
      }
    }
    if (shortcutToRemove != null) {
      const index = this.shortcuts.indexOf(shortcutToRemove, 0);
      this.shortcuts.splice(index, 1);
      Logging.log(`Remove shortcut for "${key}"`)
    }
  }

  private checkShortcuts(event: KeyboardEvent) {
    for (var shortcut of this.shortcuts) {
      if (shortcut.Key === event.key)
      {
        Logging.log(`Function called for shortcut "${shortcut.Key}".`)
        shortcut.Callback();
         }
    }
  }
}