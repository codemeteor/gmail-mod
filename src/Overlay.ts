import { Util } from './Util';

/**
 * Create an overlay that can be used to block the users interaction with the onderlying
 * interface.
 */
export class Overlay {
  private callback: Function | null = null;

  /**
   * Create and disply the overlay element.
   */
  public create() {
    let overlay = Util.createElementFromHtml(
      `<div class="Overlay">\
      </div>`
    );

    document.getElementsByTagName("body")[0].appendChild(overlay);
  }

  /**
   * Hide and remove the overlay element.
   */
  public close() {
    let overlay = <HTMLDivElement>document.querySelector(".Overlay");
    let overlayParent = <HTMLElement>overlay.parentNode;
    overlayParent.removeChild(overlay);
  }
}