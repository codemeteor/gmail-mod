/**
 * Provides utility functions.
 */
export class Util {
	/**
	 * Creates the DOM elements from a string containing HTML markup.
	 * @param {string} html The string from which to create the DOM elements.
	 */
	static createElementFromHtml(html: string): HTMLElement {
		let dom = new DOMParser().parseFromString(html.trim(), "text/html");

		let toReturn = <HTMLElement>(
			dom.documentElement.children[1].firstElementChild
		);
		return toReturn;
	}

	static showError(message: string) {
		document.body.appendChild(Util.createElementFromHtml(`<div id="gmail-mod-error" class='gmail-mod-error-message'>${message}</div>`));
		setTimeout(() => {
			let messageToDelete: HTMLDivElement = <HTMLDivElement>document.getElementById("gmail-mod-error");
			messageToDelete.parentElement?.removeChild(messageToDelete);
		}, 5000)
	}

	static httpGet(
		url: string,
		method: string = `GET`,
		responseType: XMLHttpRequestResponseType = "text"
	): Promise<any> {
		return new Promise(function (resolve, reject) {
			let request = new XMLHttpRequest();
			request.responseType = responseType;
			request.open(method, url);
            request.onload = function() {
                if (this.status >= 200  && this.status < 300) {
                    resolve(request.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: request.statusText
                    });
                }
            }
            request.onerror = function () {
                reject(
                    {
                        status: this.status,
                        statusText: request.statusText
                    }
                );
            };
            request.send();
		});
	}
}
