import { Shortcuts } from "./Shortcuts";
import { Logging } from "./Logging";

/**
 * Create a video player.
 */
export class VideoPlayer {

  constructor(private shortcuts = new Shortcuts) {
    
    shortcuts.add('Escape', () => { 
      this.close();
    });
    shortcuts.add(" ", () => {
      this.togglePlayback();
    });
    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
      let videoElement:HTMLVideoElement = <HTMLVideoElement>document.createElement("video");
      videoElement.id = "VideoElement";
      videoElement.preload = "auto";
      videoElement.controls = true;
      videoElement.autoplay = true;

      let container:HTMLDivElement = <HTMLDivElement>document.getElementById("container");
      while (container.firstChild && container.lastChild != null) {
        container.removeChild(container.lastChild);
      }
      container.appendChild(videoElement);

      videoElement.src = request.mediadata;
    });
  }

  private close() {
    window.close();
  }

  private togglePlayback() {
    const videoElement:HTMLVideoElement = <HTMLVideoElement>document.getElementById("VideoElement");
    if(videoElement.paused) {
      videoElement.play();
      Logging.log(`Play video.`);
    } else {
      videoElement.pause();
      Logging.log(`Pause video.`);
    }
  }
}

const videoPlayer = new VideoPlayer();